# PhD Package

Scripted workflow to create a compiled document with all papers for [Daniel Nüst](https://nordholmen.net/)'s PhD thesis, including an overarching introduction and synopsis.
The document includes continuous page numbering, margin notes to link to papers/chapters, highlighting of authors names and chapter links based on [Pandoc](https://pandoc.org/) filters in Python and Lua, a word cloud, a new combined figure, and a CV generated from the [public ORCID profile](https://orcid.org/0000-0002-0024-5046).

Notable used R packages (see document Colohpon for details):

- `staplr`
- `pdftools`
- `bib2df`
- `stringr`
- `tidytext`
- `wordcloud`
- `quanteda`
- `vitae`
- `rorcid`
- `ggplot2`
- `ggthemes`
- `gggalluvial`
- `jsonlite`
- `here`

The document is rendered on CI (see `.gitlab-ci.yml` for the configuration and commands) and the latest artifacts can be downloaded [here](https://zivgitlab.uni-muenster.de/d_nues01/phd-package/-/jobs/artifacts/master/download?job=default).

To include the ORCID-based CV in the CI rendering, you must define a masked variable `ORCID_TOKEN` for the build environment.
