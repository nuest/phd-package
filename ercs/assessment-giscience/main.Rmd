---
title: "Reproducible research and GIScience: an evaluation using GIScience conference papers"
author:
  - name: Frank O. Ostermann
    footnote: Corresponding author
    affiliation: "Faculty of Geo-Information Science and Earth Observation (ITC), University&nbsp;of&nbsp;Twente, Enschede, The Netherlands"
    orcid: "https://orcid.org/0000-0002-9317-8291"
  - name: Daniel Nüst
    affiliation: "Institute for Geoinformatics, University of Münster, Münster, Germany"
    orcid: "https://orcid.org/0000-0002-0024-5046"
  - name: Carlos Granell
    affiliation: "Institute of New Imaging Technologies, Universitat Jaume I de Castellón, Castellón, Spain"
    orcid: "https://orcid.org/0000-0003-1004-9695"
  - name: Barbara Hofer
    affiliation: "Christian Doppler Laboratory GEOHUM and Department of Geoinformatics - Z_GIS, University&nbsp;of&nbsp;Salzburg, Salzburg, Austria"
    orcid: "https://orcid.org/0000-0001-7078-3766"
  - name: Markus Konkol
    affiliation: "Faculty of Geo-Information Science and Earth Observation (ITC), University&nbsp;of&nbsp;Twente, Enschede, The Netherlands"
    orcid: "https://orcid.org/0000-0001-6651-0976"
date: "June 15th, 2021"
licenses:
  code: Apache-2.0
  data: ODC-BY-1.0
  text: CC-BY-4.0
output:
  html_document: default
abstract: |
  GIScience conference authors and researchers face the same computational reproducibility challenges as authors and researchers from other disciplines who use computers to analyse data.
  Here, to assess the reproducibility of GIScience research, we apply a rubric for assessing the reproducibility of over 70 conference papers published at the GIScience conference series in the years 2012-2018.
  Since the rubric and process were previously applied to the publications of the AGILE conference series, this paper itself is an attempt to replicate that analysis, however going beyond the previous work by evaluating and discussing proposed measures to improve reproducibility in the specific context of the GIScience conference series.
  The results of the GIScience paper assessment are in line with previous findings:
  although descriptions of workflows and the inclusion of the data and software suffice to explain the presented work, in most published papers they do not allow a third party to reproduce the results and findings with a reasonable effort.
  We summarise and adapt previous recommendations for improving this situation and propose the GIScience community to start a broad discussion on the reusability, quality, and openness of its research.
  Further, we critically reflect on the process of assessing paper reproducibility, and provide suggestions for improving future assessments.
---

> **ERC based on code published at <https://github.com/nuest/reproducible-research-at-giscience> and archived at <https://doi.org/10.5281/zenodo.4032875> for the paper**:
> 
> Ostermann F, Nüst D, Granell C, Hofer B, Konkol M. 2020. _Reproducible research and GIScience: an evaluation using GIScience conference papers_. EarthArXiv. <https://doi.org/10.31223/X5ZK5V>

```{r render, include=FALSE, eval=FALSE}
# render with desired output file name
rmarkdown::render("main.Rmd", output_file = "display.html")
zip::zip(zipfile = "../assessment-giscience.zip", files = list.files("."))
```

```{css, echo=FALSE}
pre {
  font-size: 12px;
  overflow-x: auto;
}
pre code {
  word-wrap: normal;
  white-space: pre;
}
```

<!-- https://github.com/nuest/reproducible-research-at-giscience -->

## License

This document is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).

All contained code is licensed under the [Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/).

The data used is licensed under a [Open Data Commons Attribution License](https://opendatacommons.org/licenses/by/).

See the paper's "Author Contributions" section for details on the contributors of data files.

## Software dpeendencies


The plots and tables of survey data and evaluation use the packages [`ggplot2`](http://ggplot2.tidyverse.org/), [`knitr::kable()`](https://yihui.name/knitr/), [`huxtable`](https://hughjonesd.github.io/huxtable/), and [`kableExtra`](https://cran.r-project.org/package=kableExtra).
Required libraries and runtime environment description are as follows.

```{r load_libraries, echo=TRUE, message=FALSE}
library("tidyverse")
library("tidyr")
library("readr")
library("ggplot2")
library("reshape2")
library("ggthemes")
library("grid")
library("gridBase")
library("gridExtra")
library("huxtable")
library("patchwork")
library("knitr")
library("ggalluvial")
library("ggfittext")
```

## Load data

```{r evaldata_file}
assessment_file <- "paper_assessment.csv"
codebook_file <- "codebook.csv"
```

The data is loaded from a collaborative spreadsheet using the [`googlesheets4`](https://googlesheets4.tidyverse.org/) package.
After a year is completed by all assessors, a [named version](https://support.google.com/a/users/answer/9331169?hl=en#) of the spreadsheet is created.
Then the data file `` `r assessment_file` `` is updated from the online spreadsheet and committed to the git history.
Afterwards the individual assessments are manually merged in the online spreadsheet and the data file is updated and committed once more.
The following plots are based on the data file `` `r assessment_file` ``, the result from the manual reproducibility assessment.
The file `` `r codebook_file` `` contains a codebook with names, labels, and descriptions for the variables in the data file as documentation of the dataset outside of this computational notebook.

```{r load_evaldata}
category_levels <- c("0", "1", "2", "3")
paper_evaluation <- readr::read_csv(assessment_file, 
    col_types = readr::cols(
      `conceptual paper` = readr::col_logical(),
      `computational environment` = readr::col_factor(levels = category_levels),
      `input data` = readr::col_factor(levels = category_levels),
      `method/analysis/processing` = readr::col_factor(levels = category_levels),
      preprocessing = readr::col_factor(levels = category_levels),
      results = readr::col_factor(levels = category_levels)
      ),
    na = "NA")
paper_evaluation_wo_conceptual <- filter(paper_evaluation, `conceptual paper` == FALSE)
categoryColumns <- c("input data", 
                     "preprocessing",
                     "method/analysis/processing",
                     "computational environment",
                     "results")
```

```{r corpus_table}
options(knitr.kable.NA = '-')
knitr::kable(paper_evaluation %>% 
               dplyr::select(-matches("X1|reviewer|conceptual|authors")),
            format = "html",
            booktabs = TRUE,
            caption = paste0("Reproducibility levels for paper corpus; ",
                             "'-' is category not available"))
```

## Table: Statistics of reproducibility levels per criterion for non-conceptual papers

```{r summary_evaldata}
evaldata_numeric <- paper_evaluation_wo_conceptual %>%
  # must convert factors to numbers to calculate the mean and median
  dplyr::mutate_if(is.factor, list(~ as.integer(as.character(.))))
# https://stackoverflow.com/questions/32011873/force-summary-to-report-the-number-of-nas-even-if-none
summaryna <- function (v) {
  if(!any(is.na(v))){
    res <- c(summary(v),"NA's"=0)
  } else{
    res <- summary(v)
  }
  return(res)
}
# apply summary independently to format as table
summaries <- sapply(evaldata_numeric[,categoryColumns], summaryna)
exclude_values_summary <- c("1st Qu.", "3rd Qu.")
kable(subset(summaries, !(rownames(summaries) %in% exclude_values_summary)), 
      digits = 1,
      col.names = c("input data", "preproc.", "method/analysis/proc.",
                    "comp. env.", "results"),
      caption = "Statistics of reproducibility levels per criterion (rounded to one decimal place)")
```

The preprocessing has `r sum(!is.na(evaldata_numeric$preprocessing))` values, with `0` and `1` around the "middle" resulting in a fraction as the median.

## Data points for text

```{r criteria_numbers}
data_level_zero <- paper_evaluation_wo_conceptual %>% 
  filter(`input data` == 0) %>% 
  count() %>% .$n
data_level_two <- paper_evaluation_wo_conceptual %>% 
  filter(`input data` == 2) %>% 
  count() %>% .$n
preprocessing_included <- paper_evaluation_wo_conceptual %>% 
  filter(!is.na(preprocessing)) %>% 
  count() %>% .$n
preprocessing_level_one <- paper_evaluation_wo_conceptual %>% 
  filter(preprocessing == 1) %>% 
  count() %>% .$n
methods_and_results_eq_one <- paper_evaluation_wo_conceptual %>% 
  filter(`method/analysis/processing` == 1 & results == 1) %>% 
  count() %>% .$n
  
compenv_level_zero <- paper_evaluation_wo_conceptual %>% 
  filter(`computational environment` == 0) %>% 
  count() %>% .$n
```

`r data_level_zero` papers have level `0` and `r data_level_two` have level `2` in the data criterion.

`r preprocessing_included` papers include some kind of preprocessing.

`r methods_and_results_eq_one` papers have level `1` in both methods and results criterion.

## Figure: Barplots of reproducibility assessment results

```{r fig_assessment_results, fig.width=12}
# match the colours to time series plot below
colours <- RColorBrewer::brewer.pal(length(categoryColumns), "Set1")
level_names <- c("0", "1", "2", "3", NA)
breaks <- seq(from = 0, to = nrow(paper_evaluation_wo_conceptual), by = 10)
criteriaBarplot = function(category, main, colour) {
  cat <- enquo(category)
  ggplot2::ggplot(data = paper_evaluation_wo_conceptual,
                aes(!!cat),
                show.legend = FALSE) +
    ggplot2::geom_bar(fill = colours[colour], color = "black") +
    ggplot2::ggtitle(main) +
    ggplot2::xlab("Level") +
    ggplot2::xlim(level_names) +
    ggplot2::ylab("") +
    ggplot2::scale_y_continuous(breaks = breaks,
                                limits = range(breaks)) +
    ggthemes::theme_tufte(base_size = 8) + theme(axis.ticks.x = element_blank())
}
fig_barplot <- patchwork::wrap_plots(
  ncol = 5,
  criteriaBarplot(`input data`,    main = "Input data",    colour = 1),
  criteriaBarplot(`preprocessing`, main = "Preprocessing", colour = 2),
  criteriaBarplot(`method/analysis/processing`,
                  main = "Methods/Analysis/\nProcessing",  colour = 3),
  criteriaBarplot(`computational environment`,
                  main = "Computational\nEnvironment", colour = 4),
  criteriaBarplot(results,         main = "Results", colour = 5)
)
fig_barplot
```

## Figure: Alluvial diagramme of reproducibility assessment results across categories

This figure is based on package [`ggalluvial`](https://corybrunson.github.io/ggalluvial/).
It does not include the category `preprocessing` because it was discovered to be quite hard to assess, and subsequently has a large share of missing values.
The figure also does _not_ include any papers who have one or more categories as `NA`, as that means "not assessable".

```{r data_alluvial}
papers_wo_na_wo_prepr <- paper_evaluation_wo_conceptual %>%
  drop_na() %>%
  mutate_if(is.factor, forcats::fct_rev) %>%
  group_by(`input data`,
           `method/analysis/processing`,
           `computational environment`,
           results) %>%
  tally() %>%
  mutate(`Category levels (#)` = paste0(`input data`, " ",
                       `method/analysis/processing`, " ",
                       `computational environment`, " ",
                       results, " (", n, ")"))
```

```{r fig_alluvial, warning=FALSE}
fig_alluvial <- ggplot(data = papers_wo_na_wo_prepr,
       aes(axis1 = `input data`,
           axis2 = `method/analysis/processing`,
           axis3 = `computational environment`,
           axis4 = results,
           y = n)
       ) +
  ggplot2::scale_x_discrete(limits = c("Input Data",
                              "Methods/\nAnalysis/\nProcessing",
                              "Computational\nEnvironment",
                              "Results"),
                   expand = c(.1, 0)
                   ) +
  xlab("Category") +
  ylab("Number of papers") +
  ggplot2::scale_fill_manual(values = c(
    RColorBrewer::brewer.pal(9, "Set1"),            # colours from the other plots
    RColorBrewer::brewer.pal(4, "Dark2")[c(1,3,4)]) # manually chosen from another palette
    ) +
  ggalluvial::geom_alluvium(aes(fill = `Category levels (#)`), width = 1/3) +
  ggalluvial::geom_stratum(alpha = 1) +
  ggfittext::geom_fit_text(stat = "stratum", aes(label = after_stat(stratum)), min.size = 1) +
  ggthemes::theme_tufte()
fig_alluvial
```

## Table: Mean levels per criterion for non-conceptual papers

```{r summary_evaldata_grouped}
means <- lapply(evaldata_numeric %>%
                      select(all_of(categoryColumns)),
                    summary) %>%
    lapply(`[[`, "Mean") %>%
    as.data.frame()
kable(means,
      digits = 2,
      col.names = c("input data", "preproc.", "method/analysis/proc.", "comp. env.", "results"),
      caption = "Mean levels per criterion for non-conceptual papers")
```

## Table: Mean levels averaged across criteria over time for non-conceptual papers

```{r evaldata_summary_by_year_mean}
means_years <- evaldata_numeric %>%
  filter(`conceptual paper` == FALSE) %>%
  group_by(year) %>%
  summarise(mean = mean(c(`input data`, 
                          preprocessing, 
                          `method/analysis/processing`, 
                          `computational environment`, 
                          `results`),
                        na.rm = TRUE),
            `paper count` = n())
means_years_table <- means_years %>% 
        mutate(mean = round(mean, 2), 
               `paper count` = as.character(`paper count`)) %>%
        mutate(labels = str_c(year, " (n = ", `paper count`, ")")) %>%
        column_to_rownames("labels") %>%
        select(mean) %>%
        t()
kable(means_years_table,
      caption = "Summarised mean values over all criteria over time (non-conceptual papers)")
```

## Figure: Mean reproducibility levels per category over time for non-conceptual papers

```{r Fig4,fig.height=4,dpi=300}
evaldata_years <- evaldata_numeric %>%
  filter(`conceptual paper` == FALSE) %>%
  group_by(year) %>%
  summarise(input = mean(`input data`, na.rm = TRUE),
         preprocessing = mean(preprocessing, na.rm = TRUE),
            method = mean(`method/analysis/processing`, na.rm = TRUE),
            environment = mean(`computational environment`, na.rm = TRUE),
            results = mean(results, na.rm = TRUE))
paper_count_years <- evaldata_numeric %>%
  filter(`conceptual paper` == FALSE) %>%
  group_by(year) %>%
  summarise(`paper count` = n())
evaldata_years_long <- melt(evaldata_years, id.vars = c("year"))
fig_mean_over_time <- ggplot(evaldata_years_long, aes(year, value)) +
  geom_bar(aes(fill = variable), position = "dodge", stat = "identity") +
  #geom_errorbar(stat = "summary", fun.data = "mean_sdl", 
  #                fun.args = list(mult = 1),
  #                position =  position_dodge(width = 0.9)) +
  ylab("mean value of criterion level") + 
  scale_x_continuous(breaks = evaldata_years$year,
                     labels = paste0(paper_count_years$year, 
                                     " (n=", 
                                     paper_count_years$`paper count`, 
                                     ")")) +
  scale_fill_brewer(palette = "Set1", name = "Category") +
  ggthemes::theme_tufte(base_size = 18) +
  theme(legend.position = c(0.15,0.75), 
        legend.text = element_text(size = 14)) +
  ylim(0, 3) +
  stat_summary(fun = mean, fun.min = mean, fun.max = mean, shape = "-", size = 2) +
  stat_summary(fun = mean, geom = "line", linetype = "dotted", mapping = aes(group = 1))
fig_mean_over_time
```

## Appendix
  
```{r corpus-table, echo=FALSE, message=FALSE, warning=FALSE, out.width='100%'}
library("knitr")
library("dplyr")

knitr::kable(paper_evaluation %>%
               select(year, title, `conceptual paper`,
                      `input data`, `preprocessing`, `method/analysis/processing`, `computational environment`, `results`),
      col.names = c("year", "title", "con-cep-tual",
                    "input data", "pre-proc.", "method/ anal./ proc.", "comp. env.", "results"),
      caption = "Assessment results excerpt; for all fields (including assessors, authors, and assessment comments) see reproducibility package at https://doi.org/10.5281/zenodo.4032875.")
```

## Colophon

```{r session_info}
sessionInfo()
```
