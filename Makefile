online:
# need to sed the setting so that the highlight-papers.py script can load it from there (env var didn't work)
	sed -i.bak 's/print: true/print: false/' phd-package.Rmd
	sed -i.bak 's/public: false/public: true/' phd-package.Rmd
	sed -i.bak 's/includePapers: false/includePapers: true/' phd-package.Rmd
	R -q -e 'rmarkdown::render("phd-package.Rmd", params = list(includePapers = TRUE, public = TRUE)); warnings()'

introsynop:
	R -q -e 'rmarkdown::render("phd-package.Rmd", params = list(includePapers = FALSE, public = TRUE), output_file = "phd-package-introsynop.pdf"); warnings()'

print:
# need to sed the setting so that the highlight-papers.py script can load it from there (env var didn't work)
	sed -i.bak 's/print: false/print: true/' phd-package.Rmd
	sed -i.bak 's/public: true/public: false/' phd-package.Rmd
	sed -i.bak 's/includePapers: false/includePapers: true/' phd-package.Rmd
	R -q -e 'rmarkdown::render("phd-package.Rmd", params = list(includePapers = TRUE, public = FALSE, print = TRUE), output_file = "temp.pdf"); warnings()'
	gs -q -dNOPAUSE -dBATCH -dPDFSETTINGS=/prepress -sDEVICE=pdfwrite -sOutputFile=phd-package-print.pdf temp.pdf
	rm -r temp.* temp_files

clean:
	rm -f -r phd-package.pdf phd-package-print.pdf phd-package-overview.pdf phd-package-introsynop.pdf data/register.json phd-package_cache *_files/
	
all: clean online print introsynop
