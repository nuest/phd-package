#!/usr/bin/env python3
"""
Pandoc filter using panflute, http://scorreia.com/software/panflute/guide.html
"""

import panflute as pf
import frontmatter

package = frontmatter.load("phd-package.Rmd")
included_papers = package["dissertation_papers"]["infrauser"] + package["dissertation_papers"]["comminpol"]
print = package["params"]["print"]

def prepare(doc):
    pass


def action(elem, doc):
    if not print and isinstance(elem, pf.Div):
        #pf.debug(elem.identifier)
        id = elem.identifier[4:]
        #pf.debug(id)
        if id in included_papers:
            text = r"\mymarginpar{\protect\hyperlink{%s}{>> Go to paper}}" % (id)
            #pf.debug("------")
            #pf.debug(elem)
            marginnote = pf.RawInline(text = text, format = "latex")
            elem.content[0].content.insert(1, marginnote)
            #pf.debug("------after")
            #pf.debug(elem)
            return elem
        else:
            return elem
        # pass
        # return [] -> delete element


def finalize(doc):
    pass


def main(doc=None):
    return pf.run_filter(action,
                         prepare=prepare,
                         finalize=finalize,
                         doc=doc) 


if __name__ == '__main__':
    main()
