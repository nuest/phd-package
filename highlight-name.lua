local utils = require 'pandoc.utils'
local stringify = utils.stringify

local underline_author_filter = {
  Para = function(el)
    if el.t == "Para" then
    for k,_ in ipairs(el.content) do
      --io.write(stringify(el.content[k]))
      if el.content[k].t == "Str" and el.content[k].text == "Nüst,"
      and el.content[k+1].t == "Space"
      and el.content[k+2].t == "Str" and el.content[k+2].text:find("^D.") then
          --io.write(stringify(el.content[k]))
          local _,e = el.content[k+2].text:find("^D.")
          local rest = el.content[k+2].text:sub(e+1) 
          el.content[k] = pandoc.Underline { pandoc.Str("Nüst, D.") }
          el.content[k+1] = pandoc.Str(rest)
          table.remove(el.content, k+2) 
      end
    end
  end
  return el
  end
}

local function starts_with(str, start)
   return str:sub(1, #start) == start
end

function Div (div)
  --io.write(div.identifier)
  if starts_with(div.identifier, "refs") then
    return pandoc.walk_block(div, underline_author_filter)
  end
  return nil
end
